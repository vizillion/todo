var configValues = require("./config");

module.exports = {
    getDbConnectionString: function(){
        return `mongodb://${configValues.username}:${configValues.password}@ds129469.mlab.com:29469/node-todos`;
    }
}