var app = angular.module("app.todos", ["xeditable"]);

app.controller("todoController", ['$scope', 'svTodos', function($scope, svTodos){

    $scope.appName = "Todo Dashboard";
    $scope.formData = {};
    $scope.loading = true;

    $scope.todos = [];

    var getTodos = function (data) {
        let objTodo = data["data"];
        $scope.todos = objTodo;
    }

    // load data from api
    svTodos.get().then(function(data){
        getTodos(data);
        $scope.loading = false;
        console.log($scope.todos);
    });

    $scope.createTodo = function() {
        $scope.loading = true;
        let todo = {
            text: $scope.formData.text,
            isDone: false
        }

        svTodos.create(todo)
            .then(function (data){
                $scope.todos = data;
                $scope.formData.text = "";
                $scope.loading = false;
                getTodos(data);
            })
        
    }

    $scope.updateTodo = function(todo) {
        console.log("Update todo: ", todo);
        $scope.loading = true;
        svTodos.update(todo)
            .then(function(data){
                getTodos(data);          
                $scope.loading = false;
            })
        
    }

    $scope.deleteTodo = function(todo){
        console.log("Delete todo: ", todo);
        $scope.loading = true;
        svTodos.delete(todo._id)
            .then(function(data) {
                getTodos(data);
                $scope.loading = false;
            });
    }

    

}]);